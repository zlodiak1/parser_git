from classes.main import Main
from classes.parser import Parser
from classes.console import Console
from classes.page import Page
from classes.file import File
from classes.db import Db
import config

if __name__ == '__main__':
    strategy = File(config.results_filename)
    # strategy = Db(config.results_dbname)

    # View = Console
    View = Page

    main = Main(View=View, Parser=Parser, config=config, strategy=strategy)

    main.run()

    

