debug = True
url = 'https://github.com/pallets/flask'
host = 'localhost'
port = 8080
element_class = 'text-emphasized'
headers = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36'}
results_filename = 'results.txt'
results_dbname = 'results'