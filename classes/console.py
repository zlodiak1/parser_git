from interfaces.interfaces import IView

class Console(IView):
    def render(self):
        self.start_parse = input('Start parse? y/n: ')

    def get_start_parse(self):
        return self.start_parse    