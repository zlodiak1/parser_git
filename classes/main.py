class Main:
    def __init__(self, View, Parser, config, strategy):
        self.view = View()
        self.strategy = strategy
        self.config = config
        self.parser = Parser(
            url=config.url, 
            element_class=config.element_class,
            headers=config.headers
        )

    def run(self):
        self.view.render()
        start_parse = self.view.get_start_parse()
        print('====', start_parse)

        if start_parse == 'y':
            data = self.parser.get_count()
            self.strategy.save(data, self.config.url)
