from bottle import route, run, request

from interfaces.interfaces import IView
import config

class Page(IView):
    def render(self):
        @route('/page')
        def page():
            return '''
                <form method="POST" action="submit">
                    <input type="hidden" name="start_parse" value="y">
                    <input type="submit" value="Start parse">
                </form>
            '''

        @route('/submit', method='POST')
        def submit():
            self.start_parse = request.forms.get('start_parse')
            print('submit', self.start_parse)

        run(host=config.host, port=config.port, debug=config.debug)

    def get_start_parse(self):
        print('get_start_parse')
        return self.start_parse
            