from interfaces.interfaces import ISave

import datetime
import sqlite3


class Db(ISave):
    def __init__(self, results_dbname):
        self.results_dbname = results_dbname
        self.conn = sqlite3.connect(self.results_dbname)
        self.cursor = self.conn.cursor()        

    def save(self, data, url):
        now = datetime.datetime.now()
        self.cursor.execute(
            '''insert into results (commits, url, date) values (?, ?, ?);''', 
            (str(data), str(url), now.strftime("%Y-%m-%d %H:%M"))
        )
        self.conn.commit()