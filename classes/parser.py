import requests
from bs4 import BeautifulSoup
import re


class Parser:
    def __init__(self, url, element_class, headers):
        self.url = url
        self.element_class = element_class
        self.headers = headers

    def get_count(self):
        root_html = self.get_html(self.url)
        count = self.find_count(root_html)
        return count

    def get_html(self, url):
        html = requests.get(url, headers=self.headers).text
        return html

    def find_count(self, html):
        soup = BeautifulSoup(html, 'lxml')
        count_obj = soup.find('span', {'class': self.element_class})
        count = re.sub("\D", "", count_obj.text)
        return int(count)
