from interfaces.interfaces import ISave

import datetime


class File(ISave):
    def __init__(self, results_filename):
        self.results_filename = results_filename

    def save(self, data, url):
        with open(self.results_filename, 'a', encoding='utf-8') as f:
            now = datetime.datetime.now()
            f.write(now.strftime("%Y-%m-%d %H:%M") + ' :: ' + str(data) + ' :: ' + url + '\n')
