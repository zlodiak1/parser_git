from abc import ABCMeta, abstractmethod

class IView(metaclass=ABCMeta):
    @abstractmethod
    def render(self):
        pass

    @abstractmethod
    def get_start_parse(self):
        pass


class ISave(metaclass=ABCMeta):
    @abstractmethod
    def save(self, data, url):
        pass
   